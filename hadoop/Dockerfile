FROM java:8-jre

RUN DEBIAN_FRONTEND=noninteractive \
    sed -i 's/deb\.debian\.org/mirrors.aliyun.com/g' /etc/apt/sources.list && \
    sed -i 's/security\.debian\.org/mirrors.aliyun.com\/debian-security/g' /etc/apt/sources.list && \
    apt-get update -q && \
    apt-get install -q -y curl \
                          zlib1g \
                          libstdc++6 \
                          libgcc1 \
                          libsnappy1 \
                          libssl-dev \
                          libc6 && \
    rm -R /var/lib/apt/*

ARG HADOOP_VERSION=2.8.2
ENV HADOOP_URL https://mirrors.aliyun.com/apache/hadoop/common/hadoop-$HADOOP_VERSION/hadoop-$HADOOP_VERSION.tar.gz
RUN set -x \
    && curl -fSL "$HADOOP_URL" -o /tmp/hadoop.tar.gz \
    && tar -xvf /tmp/hadoop.tar.gz -C / \
    && rm /tmp/hadoop.tar.gz*

RUN ln -s /hadoop-$HADOOP_VERSION/etc/hadoop /etc/hadoop
RUN cp /etc/hadoop/mapred-site.xml.template /etc/hadoop/mapred-site.xml
RUN mkdir /hadoop-$HADOOP_VERSION/logs

ENV HADOOP_PREFIX=/hadoop-$HADOOP_VERSION
ENV HADOOP_CONF_DIR=/etc/hadoop
ENV MULTIHOMED_NETWORK=1

ENV USER=root
ENV PATH $HADOOP_PREFIX/bin/:$PATH

ADD entrypoint.sh /entrypoint.sh
RUN chmod a+x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
