#!/bin/bash

set -x && \
    curl -fSL "${PROTOBUF_URL}" -o /tmp/protobuf.tar.gz && \
    tar -xvf /tmp/protobuf.tar.gz -C /tmp/ && \
    rm /tmp/protobuf.tar.gz && \
    cd /tmp/protobuf-${PROTOBUF_VERSION} && \
    autoconf -f -i -Wall,no-obsolete && \
    ./configure --prefix=/usr --enable-static=no && \
    make -j2 && \
    make install

set -x && \
    curl -fSL "${HADOOP_URL}" -o /tmp/hadoop.tar.gz && \
    tar -xvf /tmp/hadoop.tar.gz -C /tmp/ && \
    rm /tmp/hadoop.tar.gz && \
    cd /tmp/hadoop-${HADOOP_VERSION}-src && \
    mvn package -Pdist,native -DskipTests -Dtar && \
    cp -R hadoop-common-project/hadoop-common/target/native/target/* /hadoop-native/ && \
    cp -R hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-nodemanager/target/native/target/* /hadoop-native/ && \
    cp -R hadoop-hdfs-project/hadoop-hdfs-native-client/target/native/target/* /hadoop-native/ && \
    cp -R hadoop-tools/hadoop-pipes/target/native/libhadoop* /hadoop-native/usr/local/lib/



# vi /tmp/hadoop-2.8.2-src/hadoop-common-project/hadoop-common/src/main/native/src/exception.c
#  take return stderr() only
# vi /tmp/hadoop-2.8.2-src/pom.xml
# exclude last 5 modules